﻿using System;

namespace LerenWerkenMetVariabelenEnDatatypes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            // een variabele declareren: identifier (naam) en gegevenstype
            string voornaam;
            // een waarde toekennen aan een variabele
            voornaam = "Mo";
            // variabele declareren en onmiddellijk een waarde eraan toekennen
            string familienaam = "El Farisi";
            // lengte in cm
            int lengte = 178;
            // compacter
            byte compacteLengte = 178;
            // rekening nummer
            string rekeningnummer = "123456789";
            // saldo op zijn rekening
            float saldo = 1250.55f;

            // getal pi, grote precisie
            decimal pi = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679m;
            // toon in de console
            Console.Write("Voornaam: ");
            Console.Write(voornaam);
            Console.Write("Familienaam: ");
            Console.WriteLine(familienaam);
            Console.Write("Lengte: ");
            Console.Write(lengte);
            Console.Write("Compacte Lengte: ");
            Console.WriteLine(compacteLengte);
            Console.Write("Rekeningnummer: ");
            Console.WriteLine(rekeningnummer);
            Console.Write("Saldo: ");
            Console.WriteLine(saldo);
            Console.Write("PI: ");
            Console.WriteLine(pi);

            Console.Write("Resultaat: ");
            Console.WriteLine(2 + 3 * 5);
            Console.Write("Resultaat: (2+3)*5");
            Console.WriteLine((2 + 3) * 5);
            // Tip: gebruik altijd haakjes
            // zo geef je zelf aan welke bewerking er 
            // eerst moet worden uitgevoerd
            Console.Write("Resultaat: 2 + (3 * 5): ");
            Console.WriteLine(2 + (3 * 5));
            Console.Write("Resultaat zonder haakjes: 2 + 3 * 5 / 3 - 4 * 5 + 6 - 2 / 4: ");
            Console.WriteLine(2 + 3 * 5 / 3 - 4 * 5 + 6 - 2 / 4);
            // haakjes volgens de volgorde van de bewerkingen
            Console.Write("Resultaat met haakjes: ((2 + ((3 * 5) / 3)) - (4 * 5)) + (6 - (2 / 4)): ");
            Console.WriteLine(((2 + ((3 * 5) / 3)) - (4 * 5)) + (6 - (2 / 4)));
            Console.Write("Verkeerc Resultaat met haakjes: ((2 + ((3 * 5) / 3)) - (4 * 5)) + (6 - (2 / 4)): ");
            Console.WriteLine(((2 + (3 * 5) / 3) - (4 * 5)) + (6 - (2 / 4)));
            Console.WriteLine(2 + (3 * 5));
            Console.Write("Resultaat: 2 + 3 * 5 / 3 - 4 * ((5 + (6 - 2)) / 4)");
            Console.WriteLine(2 + (3 * 5));

            // vermengen van datatypes
            char eersteLetter = 'M';
            // char word impliciet gecast (omgezet) naar string
            string naam = eersteLetter + ". " + familienaam;
            Console.Write("De naam is: ");
            Console.WriteLine(naam);
            // het volgende lukt niet, een string kan niet impliciet omgezet
            // worden naar een char (is veel te klein)
            // char volledigeNaam = voornaam + ' ' + familienaam;
            // en een int naar char?
            // eersteLetter = lengte; nee dus
            // een int naar string wordt impliciet omgezet; dwz. je moet
            // dat niet zelf doen, C# doet dit voor u vanzelf
            string naamEnLengte = voornaam + ' ' + familienaam + ' ' + lengte;
            // ik kan ook een reeds gedeclareerde variabele gebruiken:
            naam = voornaam + ' ' + familienaam + ' ' + lengte;
            Console.WriteLine(naam);
            // ook een float wordt impliciet gecast naar string
            naam = "saldo van " + voornaam + ' ' + familienaam + " is: " + saldo;
            Console.WriteLine(naam);
            // uint naar float?
            naam = "saldo van " + voornaam + ' ' + familienaam + " is: " + (saldo + 50u);
            Console.WriteLine(naam);
            // van float naar int?
            // lengte = lengte + 5.5f; niet dus

            naam = voornaam + ' ' + familienaam + " is gegroeid: " + lengte;
            Console.WriteLine(naam);

            double helft = 10000.0 * (1.0d / 2);
            Console.WriteLine("Je krijgt: " + helft);

            // verkorte expressies
            int getal = 10;
            getal++; // getal = getal + 1
            Console.WriteLine("getal++: " + getal);
            ++getal; // getal = getal + 1
            getal--; // getal = getal -1
            Console.WriteLine("getal--: " + getal);
            --getal; // getal = getal - 1
            Console.WriteLine("--getal: " + getal);
            getal += 4; // getal = getal + 4
            Console.WriteLine("getal += 4: " + getal);
            getal -= 5; // getal = getal - 5
            Console.WriteLine("getal -= 5: " + getal);
            getal *= 2; // getal = getal * 2
            Console.WriteLine("getal *= 2: " + getal);
            getal /= 3; // getal = getal / 3 
            Console.WriteLine("getal /= 3: " + getal);
            Console.ReadLine();

            // Ctrl K-D om je code mooi te laten uitlijnen
        }
    }
}
