﻿using System;

namespace Nesting
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Nesting");
            byte getal1 = 2;
            byte getal2 = 4;
            if (getal1 == 2)
            {
                if (getal2 == 4)
                {

                }
            }

            if (getal1 == 2 && getal2 == 4)
            {

            }

            if (getal1 == 2)
            {
                getal2++;
                if (getal2 == 4)
                {

                }
            }


        }
    }
}
