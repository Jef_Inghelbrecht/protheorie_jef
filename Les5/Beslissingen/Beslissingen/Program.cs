﻿using System;

namespace Beslissingen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Beslissingen!");
            int nummer = 3;
            if (nummer < 5)
            {
                Console.WriteLine($"{nummer} is kleiner dan 5");
            }
            // Als er in de if code blok maar 1 regel of 1
            // statement staat, moeten die niet tussen
            // accolades staan
            nummer = 12;
            if (nummer < 5)
                Console.WriteLine($"{nummer} is kleiner dan 5");
            Console.WriteLine("Hello!");

            // Antwoord op de vraag van andrea
            // Als de expressie false is wordt het antwoord
            // in het groen getoond
            // Als de epxressie true is in het rood
            Console.Write("Typ een getal groter dan 5 in: ");
            int input = Convert.ToInt32(Console.ReadLine());
            if (input > 5)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            } else
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }
            Console.WriteLine($"Het ingetypte getal is {input}");

            Console.ReadKey();
        }
    }
}
