﻿using System;

namespace Scope
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Scope");
            int getal1 = 20;
            if (getal1==20)
            {
                int getal2 = 20;
                Console.WriteLine($"Getal 2 is {getal2}");
                getal1 = 100;
            }
            // getal2 is onbekend omdat die buiten bereik van if codeblok ligt
            // Console.WriteLine($"Getal 2 is {getal2}");
            Console.WriteLine($"Getal 1 is {getal1}");

        }
    }
}
