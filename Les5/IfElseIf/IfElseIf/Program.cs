﻿using System;

namespace IfElseIf
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("If else if");
            Random random = new Random();
            int getal = random.Next(1, 8);
            int getal2 = random.Next(1, 8);
            //if (getal == 1)
            //{
            //    Console.Beep();
            //} 
            //else if(getal == 2)
            //{
            //    Console.Beep();
            //    Console.Beep();
            //}
            //else if (getal == 3)
            //{
            //    Console.Beep();
            //    Console.Beep();
            //    Console.Beep();
            //}
            //else if (getal == 4)
            //{
            //    Console.Beep();
            //    Console.Beep();
            //    Console.Beep();
            //    Console.Beep();
            //}
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"Het gegenereerde getal is {getal}");
            if (getal == 1 && getal2 == 1)
            {
                Console.Beep();
            }

            switch (getal)
            {
                case 1:
                    Console.Beep();
                    break;
                case 2:
                    Console.Beep();
                    Console.Beep();
                    break;
                case 3:
                    Console.Beep();
                    Console.Beep();
                    Console.Beep();
                    break;
                case 4:
                    Console.Beep();
                    Console.Beep();
                    Console.Beep();
                    Console.Beep();
                    break;
                default:
                    Console.WriteLine("Geen getal tussen 1 en 4");
                    break;
            }
            Console.ReadKey();

        }
    }
}
