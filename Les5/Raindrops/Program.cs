﻿using System;

namespace Raindrops
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Raindrops");
            Console.Write("Typ een positief geheel getal:");
            uint getal = Convert.ToUInt16(Console.ReadLine());
            if ((getal % 3) == 0)
            {
                Console.Write("Pling\n");

            }
            else if ((getal % 5) == 0)
            {
                Console.Write("Plang\n");
            }
            else if ((getal % 7) == 0)
            {
                Console.Write("Plong\n");
            }
            Console.ReadKey();
            if ((getal % 3) == 0 || (getal % 5) == 0 || (getal % 7) == 0)
            {
                if ((getal % 3) == 0)
                {
                    Console.Write("Pling");

                }
                if ((getal % 5) == 0)
                {
                    Console.Write("Plang");
                }
                if ((getal % 7) == 0)
                {
                    Console.Write("Plong");
                }
            }
            else
            {
                Console.WriteLine(getal);
            }
            Console.ReadKey();
            // derde oplossing, heeft mijn voorkeur
            // a sentinel construct
            bool getoond = false;
            if ((getal % 3) == 0)
            {
                Console.Write("Pling");
                getoond = true;

            }
            if ((getal % 5) == 0)
            {
                Console.Write("Plang");
                getoond = true;
            }
            if ((getal % 7) == 0)
            {
                Console.Write("Plong");
                getoond = true;
            }

            if (!getoond)
            {
                Console.WriteLine(getal);
            }


            Console.ReadKey();
        }
    }
}
