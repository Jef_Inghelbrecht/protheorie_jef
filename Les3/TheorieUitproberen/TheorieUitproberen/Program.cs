﻿using System;

namespace TheorieUitproberen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            string voornaam = "Karel";
            char singleQuote = '\'';

            Console.WriteLine(singleQuote + voornaam + singleQuote);
            Console.WriteLine("Ik ga naar de film \"Harry Potter\"");
            Console.WriteLine("Ik ga naar de film" + '"' + "Harry Potter" + '"');
            // 
            Console.WriteLine("Klein klein kleutertje");
            Console.WriteLine("Wat doe je in mijn hof");
            Console.WriteLine("Je plukt er alle bloempjes af");
            Console.WriteLine("Je maakt het veel te grof");
            Console.WriteLine("Klein klein kleutertje\n\tWat doe je in mijn hof\n\tJe plukt er alle bloempjes af\n\tJe maakt het veel te grof");
            Console.WriteLine("10\t20\t30\n40\t50\t60");
            voornaam = "Mo";
            string familienaam = "El Farisi";
            // interpolatie, pas sinds C# 6
            Console.WriteLine($"Je voornaam is {voornaam} en je familienaam is {familienaam}.");
            Console.ReadKey();
        }
    }
}
