﻿using System;

namespace MijnHello
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello mijn eerste les programmeren!");
            Console.Write("Hoe heet jij? ");
            // variable declareren: gegevenstype + identifier (naam)
            string inputEigennaam;
            // variable initialiseren: een waarde eraan toekennen (assignment)
            inputEigennaam = Console.ReadLine();
            Console.Write("Je naam is ");
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(inputEigennaam);
            // label en input op 1 lijn plaatsen
            // dan moeten we strings aan elkaar plakken
            Console.WriteLine("Nog eens, je naam is " + inputEigennaam); 
            Console.ReadLine();
        }
    }
}
