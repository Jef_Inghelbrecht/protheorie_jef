﻿using System;

namespace JanHuygh
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Jan Huygh");
            Console.Write("Typ een komma getal in:");
            string input = Console.ReadLine();
            double getal;
            bool done = Double.TryParse(input, out getal);
            Console.WriteLine($"getal is: {getal}");
            Console.ReadKey();

        }
    }
}
