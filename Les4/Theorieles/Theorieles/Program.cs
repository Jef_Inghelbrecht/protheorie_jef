﻿using System;

namespace Theorieles
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Casting, Conversion en Parsing");
            int getal1 = 2;
            float getal2 = 1.00f;
            double getal3;
            getal3 = getal1; // widening impliciet uitgevoerd
            getal1 = (int) getal2;
            string naam = "Mo";
            //getal1 = (int) naam;
            char eenKarakter = 'A';
            getal1 = eenKarakter;
            //naam = eenKarakter;
            naam = "3M";
            string rekeningnummer = "00345678234";
            //int rekeningnummerGetal = (int)rekeningnummer;
            int rekeningnummerGetal = Convert.ToInt32(rekeningnummer);
            Console.WriteLine($"rekeningnummer van string naar int: {rekeningnummerGetal}");
            //getal1 = Convert.ToInt32(naam);
            Console.WriteLine($"De naam Mo van string naar int: {getal1}");
            bool isHetMogelijk = Int32.TryParse(naam, out getal1);
            Console.Write("Hoeveel weeg je? ");
            string answer = Console.ReadLine();
            bool isHetEenGetal = Int32.TryParse(answer, out getal1);
            Console.WriteLine($"Heb je een getal ingetypt? {isHetEenGetal}");
            Console.ReadKey();
        }
    }
}
