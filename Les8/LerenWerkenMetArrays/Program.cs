﻿using System;

namespace LerenWerkenMetArrays
{
    class Program
    {
        static void Main(string[] args)
        {

            // OpgepastMet0Index2();
            string[] voornamen = MaakVoornaamArray();
            PrintStringArray(voornamen);
            string[] familienamen = MaakFamilienaamArray();
            PrintStringArray(familienamen);
            Console.ReadKey();
        }

        static void DeEersteOefening()
        {
            Console.WriteLine("Leren werken met arrays");
            // het nut van arrays
            // zonder array's moet ik voor elke persoon een variabele declareren
            byte leeftijdFarah = 17;
            byte leeftijdBrian = 20;
            byte leeftijdMuhammed = 24;
            byte leeftijdAyoub = 19;
            byte leeftijdJef = 63;
            // bereken gemiddelde
            Console.WriteLine($"Gemiddelde leeftijd is: {(leeftijdFarah + leeftijdBrian + leeftijdMuhammed + leeftijdAyoub + leeftijdJef) / 5}.");
            // declareer array
            // en initialiseer array met 5 int waarden
            byte[] leeftijd = { 17, 20, 24, 19, 63 };
            // bereken gemiddelde leeftijd
            // declareer som buiten de for blok
            // want we hebben die na de for nodig
            // om het gemiddelde te berekenen en te tonen
            int som = 0;
            for (byte i = 0; i < leeftijd.Length; i++)
            {
                som += leeftijd[i];
            }
            Console.WriteLine($"Gemiddelde leeftijd is: {som / leeftijd.Length}.");
            // declareer namen array en initialiseer die met vijf namen
            string[] namen = new string[] { "Julia", "Peter", "Pun", "Ben", "Sumaya" };
            // Vraag naar de leeftijd voor elke naam en stop
            // die in een overeekomstig element in de leeftijd array
            for (int i = 0; i < namen.Length; i++)
            {
                Console.Write($"Hoe oud is {namen[i]}? ");
                leeftijd[i] = Convert.ToByte(Console.ReadLine());
            }
            // sentinel
            byte teller = 0;
            while (teller < namen.Length)
            {
                Console.WriteLine($"{namen[teller]} is {leeftijd[teller++]} oud.");
            }
        }

        static void OpGepastMet0Index()
        {
            int[] getallen = new int[5];
            for (int i = 0; i <= 4; i++)
            {
                getallen[i] = i;
            }
        }

        static void OpgepastMet0Index2()
        {
            int[] getallen = new int[5];
            getallen[0] = 0;
            getallen[1] = 1;
            getallen[2] = 2;
            getallen[3] = 3;
            getallen[4] = 4;

            Console.WriteLine("De getallenarray bevat: ");
            for (int i = 0; i <= getallen.Length-1; i++)
            {
                Console.Write($" {getallen[i]}, ");
            }

            Console.WriteLine("De getallenarray bevat in omgekeerde volgorde: ");
            for (int i = getallen.Length - 1; i >= 0; i--)
            {
                Console.Write($" {getallen[i]}, ");
            }
            // sentinel
            Console.WriteLine("De getallenarray bevat (met een while) : ");
            int position = 0;
            while (position <= getallen.Length  - 1)
            {
                Console.Write($" {getallen[position--]}, ");
            }

            // sentinel
            Console.WriteLine("De getallenarray bevat (met een while) in omgekeerde volgorde: ");
            int lengte = getallen.Length - 1;
            while (lengte >= 0 )
            {
                Console.Write($" {getallen[lengte--]}, ");
            }

           
        }
        static string[] MaakVoornaamArray()
        {
            string[] voornaam = { "Jan", "Abdel", "Sarah", "Pieter" };
            return voornaam;
        }

        static string[] MaakFamilienaamArray()
        {
            string[] familienaam = { "Jansens", "Farisi", "De Bolle", "Goomans", "Potter", "Sponge Bob" };
            return familienaam;
        }

        static void PrintStringArray(string[] stringArray)
        {
            Console.WriteLine("De stringarray bevat: ");
            for (int i = 0; i <= stringArray.Length - 1; i++)
            {
                if (i < stringArray.Length - 1)
                {
                    Console.Write($"{stringArray[i]}, ");
                }
                else
                {
                    Console.Write(stringArray[i]);

                }
            }
        }

        static void TwoDimArray()
        {
            string[,] personen = {{"Harry", "Potter", "Bruin", "12" },
            {"Mohamed", "El Farasi", "Bruin", "45" } ,
            {"Farah", "Rahou", "Bruin", "17" } ,
            {"Jos", "Goossens", "Groen", "17" } };
        }
    }
}
