﻿using System;

namespace VoorbereidingExamenTheorieles
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Oefening laatste theorieles");
            string[] voornamen = VulArrayMetVoornamen();
            string tekstMetVoornamen = PrintStringArray(voornamen);
            Console.WriteLine(tekstMetVoornamen);
            Console.WriteLine("Druk op een toest om de console te sluiten.");
            Console.ReadKey();
        }

        static string[] VulArrayMetVoornamen()
        {
            const int MaxAantal = 5;
            string[] voornamen = new string[MaxAantal];
            // het volgende moet ik doen totdat er 5 geldige 
            // voornamen zijn ingetypt
            int aantalGeldigeVoornamen = 0;
            while (aantalGeldigeVoornamen < voornamen.Length)
            {
                Console.Write("Typ een voornaam in: ");
                string voornaam = Console.ReadLine();
                if (voornaam.Length > 0)
                {
                    voornamen[aantalGeldigeVoornamen] = voornaam;
                    aantalGeldigeVoornamen++;
                }
            }
            return voornamen;
        }

        static string PrintStringArray(string[] array)
        {
            string tekst = string.Empty;
            for (int i = 0; i < array.Length; i++)
            {
                tekst += $"array[i]\n";
            }
            return tekst;
        }
    }
}
