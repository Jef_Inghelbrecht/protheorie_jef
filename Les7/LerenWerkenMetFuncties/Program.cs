﻿using System;

namespace LerenWerkenMetFuncties
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leren werken met functies (methoden in oo)!");
            Console.WriteLine("Keuze 1");
            Console.WriteLine("Keuze 2");
            Console.WriteLine("Keuze 3");
            Console.WriteLine("Maak je keuze: ");
            string keuze = Console.ReadLine();
            switch (keuze)
            {
                case "1":
                    Console.WriteLine("Je hebt voor 1 gekozen.");
                    break;
                case "2":
                    Console.WriteLine("Je hebt voor 2 gekozen.");
                    break;
                case "3":
                    Console.WriteLine("Je hebt voor 3 gekozen.");
                    break;
            }

            Zeg(keuze);

            // programma die het grootste getal van twee ingegeven getallen berekent
            double getal = 1;
            while (getal != 0)
            {
                Console.WriteLine("Geef een getal in: ");
                string input = Console.ReadLine();
                getal = -1;
                if (input.Length > 0)
                {
                    getal = Convert.ToDouble(input);
                }
                getal = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Geef een getal in: ");
                double getal2 = Convert.ToDouble(Console.ReadLine());
                if (getal < getal2)
                {
                    Console.WriteLine($"het eerste getal {getal} is kleiner dan het tweede {getal2}");
                } 
                else
                {
                    Console.WriteLine($"het eerste getal {getal} is groter dan het tweede {getal2}");
                }
            }

            double getal1 = InputGetal();
            getal2 = InputGetal();

        }

        static double InputGetal()
        {
            Console.WriteLine("Geef een getal in: ");
            double getal =  Convert.ToDouble(Console.ReadLine());
            return getal;

        }

        static void Zeg(string keuze)
        {
            Console.WriteLine($"Je hebt voor {keuze} gekozen.");

        }
    }
}
