﻿using System;

namespace Inghelbrecht
{
    class Jef
    {
        public static int leeftijd = 102;
        public static string SystemInfo()
        {
            string info;
            info = $"Machinenaam: {Environment.MachineName}\nGebruikersnaam: {Environment.UserName}";
            return info;
        }

        public static string SystemInfoUppercase()
        {
            return SystemInfo().ToUpper();
        }
    }
}
