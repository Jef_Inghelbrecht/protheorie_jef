﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Functies2
{
    class Helpers
    {
        public static double Max(double getal1, double getal2)
        {
            if (getal1 < getal2)
            {
                return getal2;
            }
            else
            {
                return getal1;
            }
        }

        // methoden (functies)
        public static double InputGetal()
        {
            double getal = 0;
            Console.WriteLine("Typ een getal in: ");
            string input = Console.ReadLine();
            if (input.Length > 0)
            {
                getal = Convert.ToDouble(input);

            }
            return getal;
        }
    }
}
